<?php
/**
 * Setup environment
 */
@session_start();
@ob_start();
@ob_implicit_flush(0);
@error_reporting(E_ALL ^ E_NOTICE);
@ini_set('display_errors', true);
@ini_set('html_errors', false);
@ini_set('error_reporting', E_ALL ^ E_NOTICE);

@header("Content-type: text/html; charset=utf-8");

/** Define antihack property */
define('kabinet', true);

require_once(dirname(__DIR__) . '/conf.php');

require_once MOD_DIR . "/class/mysql.php";
require_once MOD_DIR . "/class/db.conf.php";
require_once MOD_DIR . '/class/templates.class.php';

$tpl = new dle_template();
$tpl->dir = ROOT_DIR . "/template/";
require_once MOD_DIR . '/function.php';

$member_id['group'] = 5;
require_once MOD_DIR . '/auth.php';

require_once(MOD_DIR . '/class/connect.class.php');
$connect = new WinConnect();

/* @var string $login_hash */

if (!isset($_POST['h']) && $_POST['h'] != $login_hash) {
    die("ERROR");
}