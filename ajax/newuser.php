<?php

require_once(__DIR__.DIRECTORY_SEPARATOR.'ajax_header.php');

/* @var db $db */
/* @var array $member_id */
/* @var WinConnect $connect */

if ($_POST['login']) {
    if ($member_id['group_id'] == 1 && $_POST['company']) {
        $member_id['company'] = intval($_POST['company']);
        $company = $db->super_query("SELECT * FROM " . PREFIX . "_company c where c.id='{$member_id['company']}' ");

    }
    $login = trim(strip_tags($db->safesql($_POST['login'])));
//$login=iconv("UTF-8","CP1251",$login);
    $name = trim(strip_tags($db->safesql($_POST['fio'])));
//$name=iconv("UTF-8","CP1251",$name);
    $pass1 = trim($_POST['password']);
    $pass2 = trim($_POST['password2']);

    if ($pass1 != $pass2) {
        die("Пароли не совпадают!");
    }
    if (!preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#", $pass1)) {
        die("Пароль должен содержать одну большую букву, одну цифру, одну маленькую!!");
    }

    if ($_POST['action'] == "newmanager") {
        $promo = $db->safesql(trim(strip_tags($_POST['promo'])));
        if ($promo == "") {
            $promo = genpass(4);
        }
        $group_id = 2;
        $promo = strtolower($promo);
    } else {

        $count = $db->super_query("SELECT count(*) as count FROM " . PREFIX . "_users where company='{$member_id['company']}'");
        if ($count['count'] >= $company['max_users']) {

            die("Привышено максимально кол-во пользователей");
        }

    }
    /*
    $count=$db->super_query("SELECT count(*) as count FROM ".PREFIX."_users where company='{$member_id['company']}'");
    if($count['count']>=$company['max_users'])
    {

        die("Превышено максимально кол-во пользователей");
    }
    */
    $user = $db->super_query("SELECT * FROM " . PREFIX . "_users where username='{$login}'");

    if ($user['id']) {
        die("Пользователь существует!");
    }
    $name_c = $db->super_query("SELECT * FROM " . PREFIX . "_users where name='{$name}'");
    if ($name_c['id']) {
        die("Пользователь с таким именем существует!!");
    }
    $password = md5(md5($pass1));

//$db->query("INSERT into ".PREFIX."_users (`username`,`password`,`company`,`name`)VALUES('$login','$password','{$member_id['company']}','{$name}')");
    $db->query("INSERT into " . PREFIX . "_users (`username`,`password`,`company`,`name`,`group_id`,`manager_code`)VALUES('$login','$password','{$member_id['company']}','{$name}','$group_id','$promo')");


    $members_num = $db->super_query("SELECT count(id) as count from " . PREFIX . "_users where company='{$member_id['company']}'");

    $quota = $company['quota'];
    if ($group_id != 2) {
        $date = date("d.m.Y", $company['end_date']);
        $params = array('action' => "add", "login" => "$login", "date" => $date, "password" => "$pass1", "name" => "$name", "group" => "group{$company['id']}", "quota" => $quota, "path" => "{$company['path']}");
        $connect->build($params);
    }


    echo 'ok';
}