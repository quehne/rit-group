function addnewuser(){
	$.post('/ajax/newuser.php',$("#newuser-form").serialize(),function(data){
	if(data=="ok")
	{
		data="Пользователь Создан!";
		notice(data);
		$(".modal").modal('hide');
		$('.modal').on('hidden', function () {
		window.location.reload()
});
	} else{
		notice(data);
	}
	});
	
	
}
function notice(data){
	$(".textMessage").html(data);
	$("#notice").show().delay(2000).fadeOut(300);
}
function showloading()
{
$( "#load-screen" ).fadeIn(200);
}
function terminal(uid,action)
{
var hash=$("[name=h]").val();
var company=$("[name=company]").val();
$.post("/ajax/logout.php",{uid:uid,action:action,h:hash,company:company},function(data)
{
		if(data=="ok") data="Сеанс пользователя завершен!";

	notice(data);
});
}
function confirmdel(uid){

if(confirm("Действительно хотите удалить данного пользователя?"))
{
	//window.location="http://"+location.host+url;
	var hash=$("[name=h]").val();
	var company=$("[name=company]").val();
	$.post("/ajax/logout.php",{uid:uid,action:'delete',h:hash,company:company},function(data)
	{
	if(data=="ok") data="Пользователь Удален!";
	notice(data);
	$("tr#"+uid).fadeOut('200');
});
}
	
}
$(function(){
var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

$('#date').datepicker({
  onRender: function(date) {
    return date.valueOf() < now.valueOf() ? 'disabled' : '';
  }
})

$( document ).ajaxStart(function() {
  $( "#load-screen" ).fadeIn(200);
});
$( document ).ajaxComplete(function() {
  $( "#load-screen" ).fadeOut(200);
});
	$('#newuser-form').validate({
        rules: {
            fio: {
                minlength: 2,
                required: true
            },
            login: {
                required: true
            },
            password: {
                minlength: 8,
                required: true
            },       
			password2: {
				required: true,
				minlength: 8,
				equalTo: "#password"
			}
        },
        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element.text('').addClass('valid')
                .closest('.control-group').removeClass('error').addClass('success');
        }
   });


})