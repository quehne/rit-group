<script type="text/javascript">
function del(url,ok)
{
    if(url && ok)
    {
        $('#order_id').html("<strong>"+ok+"</strong>");
      $("#delModal").modal();
      $("#del").click(function(){
        document.location=url;
      });
    }
    
}
</script>
<!-- Button to trigger modal -->
<!-- Modal -->
<div class="modal" style="display:none;" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Удаление заказа!</h3>
  </div>
  <div class="modal-body">
    <p>Действительно хотите удалить заказ №<span id="order_id"></span>…</p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-loading-text="Loading..." data-dismiss="modal" aria-hidden="true">Отмена</button>
    <button class="btn btn-danger" id="del" >Удалить</button>
  </div>
</div>
<table class="table table-hover table-bordered">
	<thead>
		<tr><th><a href="index.php?sort=4">№ Заказа</a></th><th>Товар\производитель</th><th><a href="index.php?sort=6">Цена</a></th><th>Услуги <br> (доставка/ занос/ сборка)</th><th>Адрес</th><th>Телефон</th><th>ФИО</th><th><a href="index.php?sort=1">Срок Доставки</a></th><th>Заметки</th><th>Действия</th></tr>

	</thead>
	<tbody>
    {rows}
    </tbody>
</table>