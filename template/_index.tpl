<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<script src="js/jquery-1.6.3.min.js"></script>
		<script src="js/value.js"></script>
		<script src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script type="text/javascript" language="javascript">
            $(function() {

                //	Scrolled by user interaction
                $('#carousel').carouFredSel({
                    auto: 10000,
                    pagination: ".pagination-list",
                    mousewheel: true,
                    swipe: {
                        onMouse: true,
                        onTouch: true
                    }
                });
				//	Scrolled by user interaction
                $('#carousel-tab').carouFredSel({
                    auto: 10000,
					pagination: ".pagination-main",
                    mousewheel: true,
                    swipe: {
                        onMouse: true,
                        onTouch: true
                    }
                });

            });
 
            function notice(data){
	$(".textMessage").html(data);
	$("#notice").show().delay(2000).fadeOut(300);
}
        </script>
        <style type="text/css">
	        	.popEvent>.in {
border: 1px solid #ccc;
background: #724D62;
padding: 15px 20px;
-moz-border-radius: 5px;
-webkit-border-radius: 5px;
border-radius: 5px;
zoom: 1;
-moz-box-shadow: 0 0 10px #888;
-webkit-box-shadow: 0 0 10px #888;
box-shadow: 0 0 10px #888;
color: white;
}
	        
        </style>
        <title>Cloud RIT Group</title>
    </head>
    <body>
		<!-- Header -->
		<header class="header">
			<div class="wrapper">
				<div class="header-top">
					<a class="logo" href="/">
						<img src="images/logo.png" alt="">
					</a>
					<h1>Программа 1С онлайн - удобное решение для Вашего бизнеса</h1>
					<div class="contacts">
						<p class="number">+ 7(495) 617-61-37</p>
						<p class="number mail">cloud@rit-group.ru</p>
					</div>
				</div>
				<div class="intro">
					<h2><span class="for">для</span> кого?</h2>
					<div class="content-slider list_carousel" id="content-slider_list_carousel">
						<div class="carusel-tab" id="carousel-tab">
							<ul class="nav">
								<li class="first">
									<a class="title"  href="/"><span>стартапам</span></a>
									<p>Готовая к работе система учета 
									уже на старте Вашего бизнеса <br>
									без капитальных <br>
									вложений.</p>
								</li>
									<li class="third">
									<a class="title" href="/"><span>бухгалтерам</span></a>
									<p>Для работы достаточно просто войти в интернет.</p>
									<a class="arrow-button" href="/"></a>
									<ul class="dropbox">
										<li><a href="/">Поддержка обслуживания нескольких клиентов одновременно.</a></li>
										<li><a href="/">Возможность расширения услуги при необходимости.</a></li>
										<li><a href="/">Время обновление 1С считанные минуты. </a></li>
									</ul>
								</li>
								<li class="second">
									<a class="title"  href="/"><span>компаниям</span></a>
									<p>Объедините офисы в единую
									систему без затрат и<br>
									настроек.</p>
								</li>
								<li class="average">
									<a class="title"  href="/"><span>интернет-магазинам</span></a>
									<p>Вам не нужен отдельный офис 
									с сервером для 1С, вся ИТ-система 
									теперь в облаке.</p>
								</li>
							</ul>
							<ul class="nav">
								<li class="five last">
									<a class="title"  href="/"><span>фирмам</span></a>
									<p>Переходящим на 1С 8 реальная
									экономия на лицензиях и 
									модернизации старого <br>
									сервера</p>
								</li>
								<li class="second">
									<a class="title"  href="/"><span>компаниям</span></a>
									<p>Объедините офисы в единую
									систему без затрат и<br>
									настроек.</p>
								</li>
								<li class="first">
									<a class="title"  href="/"><span>стартапам $</span></a>
									<p>Готовая к работе система учета 
									уже на старте Вашего бизнеса <br>
									без капитальных <br>
									вложений.</p>
								</li>
								<li class="third">
									<a class="title" href="/"><span>бухгалтерам</span></a>
									<p>Для работы достаточно просто войти в интернет.</p>
									<a class="arrow-button" href="/"></a>
									<ul class="dropbox">
										<li><a href="/">Поддержка обслуживания нескольких клиентов одновременно.</a></li>
										<li><a href="/">Возможность расширения услуги при необходимости.</a></li>
										<li><a href="/">Время обновление 1С считанные минуты. </a></li>
									</ul>
								</li>
							</ul>
							<ul class="nav">
								<li class="first">
									<a class="title"  href="/"><span>стартапам</span></a>
									<p>Готовая к работе система учета 
									уже на старте Вашего бизнеса <br>
									без капитальных <br>
									вложений.</p>
								</li>
								<li class="third">
									<a class="title" href="/"><span>бухгалтерам</span></a>
									<p>Для работы достаточно просто войти в интернет.</p>
									<a class="arrow-button" href="/"></a>
									<ul class="dropbox">
										<li><a href="/">Поддержка обслуживания нескольких клиентов одновременно.</a></li>
										<li><a href="/">Возможность расширения услуги при необходимости.</a></li>
										<li><a href="/">Время обновление 1С считанные минуты. </a></li>
									</ul>
								</li>
								<li class="second">
									<a class="title"  href="/"><span>компаниям</span></a>
									<p>Объедините офисы в единую
									систему без затрат и<br>
									настроек.</p>
								</li>
								<li class="average">
									<a class="title"  href="/"><span>интернет-магазинам</span></a>
									<p>Вам не нужен отдельный офис 
									с сервером для 1С, вся ИТ-система 
									теперь в облаке.</p>
								</li>
							</ul>
						</div>
						<div class="pagination-main">
							<a class="selected" href="/"></a>
							<a href="/"></a>
							<a href="/"></a>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- Header #END /-->
		<!-- Content -->
		<div class="container">
			<div class="wrapper">
				<div class="case">
					<div class="promo">
						<h3>Не пропусти!</h3>
						<div class="grid-box">
							<p><span>аренда</span> от 1 080 рублей
							Регистрируйся прямо сейчас!<br>
							14 дней <span>бесплатно.</span></p>
						</div>
					</div>
					<div class="promo second">
						<h3>Экономь!</h3>
						<div class="grid-box">
							<p><span>отсутствие затрат</span><br>
							на покупку и обслуживание<br>
							1с и серверов.</p>
						</div>
					</div>
				</div>
				<div class="box">
					<div class="form-data">
						<div class="gray-bg">
							<p>авторизация</p>
						</div>
						<form class="form-fields" method="post">
                        [not-logged]
							<p>* Чтобы получить доступ ко всем возможностям сайта, 
							Вам необходимо войти в свой аккаунт или 
							зарегестрироваться.</p>
							<input class="field" name="user" type="text" required value="Введите имя">
							<input class="field" name="pass"  type="password" required value="Введите пароль">
							<a href="/">Забыли пароль?</a>
                            <input type="hidden" value="login" name="action">
							<input class="enter-button" type="submit" value="войти">
                            [/not-logged]
                            [logged]
                            <h3 style="color:white;margin-top: 24px;margin-left: 70px;">Здравствуйте, {login}</h3>
                            <ul class="navigation" style="margin-top:20px;">
                                {menu}
                            </ul>
                            [/logged]
						</form>
					</div>
					<div class="form-data registr">
						<div class="gray-bg">
							<p>регистрация</p>
						</div>
						<form class="form-fields" method="post">
							<input class="field" name="user" type="text" required value="Ваше имя">
							<input class="field mail" name="email" type="email" required value="Ваш e-mail">
							<input class="field phone" name="phone" type="tel" required value="Ваш телефон">
							<input class="field password" name="promo" type="text" required value="Секретный код">
							<input type="hidden" name="action" value="register">
						<input class="enter-button" type="submit" value="зарегистрироваться">
						</form>
					</div>
				</div>
				<h2 class="advantage">наши  <span>преимущества</span></h2>
			</div>
		</div>
		<div class="container-slider">
			<div class="wrapper">
				<h2 class="security">безопасность</h2>
				<div class="slider-block list_carousel">
						<div class="slider-main" id="carousel">
							<div class="slider-carusel">
								<div class="slider-text">
									<h4><span class="safety">сохранность</span>  данных</h4>
									<p>	Вы получаете новый уровень 
									надежности данных. Работа 1С 
									онлайн ведется на производительных
									серверах в крупных дата-центрах 
									России, отвечающих строгим 
									требованиям безопасности,
									оборудованным системами защиты 
									от проникновения</p>
								</div>
								<div class="slider-text">
									<h4>круглосуточная <span class="safety">доступность</span></h4>
									<p>	Работоспособность 1С не 
									зависит от Вашего компьютера: база 1
									С хранится на наших серверах, и 
									достаточно воспользоваться любым 
									ПК с доступом в Интернет, чтобы 
									продолжить работу в случае выхода
									из строя компьютера.<br> 
									<span class="point">. . . . .</span>
									</p>
								</div>
								<div class="slider-text last">
									<h4> <span class="safety privacy">конфиденциальность</span>информации</h4>
									<p>	Аренда 1С на нашем сервисе
									дает высочайшую степень защиты
									данных. Доступ к базам 1С имеете 
									только Вы, никто из посторонних не
									может их получить, вся информация
									передается по шифрованному 
									SSL-каналу.
									</p>
								</div>
							</div>
							<div class="slider-carusel">
								<div class="slider-text">
									<h4><span class="safety">сохранность</span>  данных</h4>
									<p>	Вы получаете новый уровень 
									надежности данных. Работа 1С 
									онлайн ведется на производительных
									серверах в крупных дата-центрах 
									России, отвечающих строгим 
									требованиям безопасности,
									оборудованным системами защиты 
									от проникновения</p>
								</div>
								<div class="slider-text">
									<h4>круглосуточная <span class="safety">доступность</span></h4>
									<p>	Работоспособность 1С не 
									зависит от Вашего компьютера: база 1
									С хранится на наших серверах, и 
									достаточно воспользоваться любым 
									ПК с доступом в Интернет, чтобы 
									продолжить работу в случае выхода
									из строя компьютера.<br> 
									<span class="point">. . . . .</span>
									</p>
								</div>
								<div class="slider-text last">
									<h4> <span class="safety privacy">конфиденциальность</span>информации</h4>
									<p>	Аренда 1С на нашем сервисе
									дает высочайшую степень защиты
									данных. Доступ к базам 1С имеете 
									только Вы, никто из посторонних не
									может их получить, вся информация
									передается по шифрованному 
									SSL-каналу.
									</p>
								</div>
							</div>
							<div class="slider-carusel">
								<div class="slider-text">
									<h4><span class="safety">сохранность</span>  данных</h4>
									<p>	Вы получаете новый уровень 
									надежности данных. Работа 1С 
									онлайн ведется на производительных
									серверах в крупных дата-центрах 
									России, отвечающих строгим 
									требованиям безопасности,
									оборудованным системами защиты 
									от проникновения</p>
								</div>
								<div class="slider-text">
									<h4>круглосуточная <span class="safety">доступность</span></h4>
									<p>	Работоспособность 1С не 
									зависит от Вашего компьютера: база 1
									С хранится на наших серверах, и 
									достаточно воспользоваться любым 
									ПК с доступом в Интернет, чтобы 
									продолжить работу в случае выхода
									из строя компьютера.<br> 
									<span class="point">. . . . .</span>
									</p>
								</div>
								<div class="slider-text last">
									<h4> <span class="safety privacy">конфиденциальность</span>информации</h4>
									<p>	Аренда 1С на нашем сервисе
									дает высочайшую степень защиты
									данных. Доступ к базам 1С имеете 
									только Вы, никто из посторонних не
									может их получить, вся информация
									передается по шифрованному 
									SSL-каналу.
									</p>
								</div>
							</div>
						</div>
					<h3 class="service">Наша служба поддержки работает 24 часа в сутки!</h3>
					<div class="pagination-list">
						<a href="/"></a>
						<a href="/"></a>
						<a class="selected" href="/"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="content-bottom">
			<div class="wrapper">
				<div class="block-rate">
					<h3><span>тарифные</span> планы</h3>
					<table class="prices">
						<tr>
							<th>доступные сервисы</th>
							<th>Экономичный</th>
							<th>Базовый</th>
							<th>Расширенный</th>
							<th>Премиум</th>
						</tr>
						<tr>
							<td>1С:Бухгалтерия (Россия/Украина/Казахстан)</td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
						</tr>
						<tr>
							<td>1С:Управление торговлей (Россия/Украина/Казахстан)</td>
							<td></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
						</tr>
						<tr>
							<td>1С:Зарплата и Управление Персоналом</td>
							<td></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
						</tr>
						<tr>
							<td>1С:Розница</td>
							<td></td>
							<td></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
						</tr>
						<tr>
							<td>1С:Управление небольшой фирмой</td>
							<td></td>
							<td></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
						</tr>
						<tr>
							<td>1С:Комплексная автоматизация</td>
							<td></td>
							<td></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
						</tr>
						<tr>
							<td>Доступ удаленный рабочий стол</td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
						</tr>
						<tr>
							<td>Доступ через браузер</td>
							<td></td>
							<td></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
						</tr>
						<tr>
							<td>Максимальное количество пользователей</td>
							<td>5</td>
							<td>10</td>
							<td>Без ограничений</td>
							<td>Без ограничений</td>
						</tr>
						<tr>
							<td>Гарантированные параметры мощности для высоконагруженных баз</td>
							<td></td>
							<td></td>
							<td></td>
							<td><img src="images/ico/i-ok.png" alt=""></td>
						</tr>
						<tr>
							<td>Свободное место на жестком диске</td>
							<td>3 ГБ</td>
							<td>4 ГБ</td>
							<td>5 ГБ</td>
							<td>60 ГБ</td>
						</tr>
						<tr class="pre-last-row">
							<td></td>
							<td><span class="red">1 080 <img src="images/ico/i-money.png" alt=""></span>в месяц</td>
							<td><span class="red">1 190 <img src="images/ico/i-money.png" alt=""></span>в месяц</td>
							<td><span class="red">1 300 <img src="images/ico/i-money.png" alt=""></span>в месяц</td>
							<td><span class="red">25 700 <img src="images/ico/i-money.png" alt=""></span>в месяц</td>
						</tr>
						<tr class="last-row">
							<td></td>
							<td>Последующие пользователи: <span>860 <img src="images/ico/i-money.png" alt=""></span></td>
							<td>Последующие пользователи: <span>970 <img src="images/ico/i-money.png" alt=""></span></td>
							<td>Последующие пользователи: <span>1 135 <img src="images/ico/i-money.png" alt=""></span></td>
							<td>Последующие пользователи: <span>1 650 <img src="images/ico/i-money.png" alt=""></span></td>
						</tr>
					</table>
					<h4>дополнительные опции</h4>
					<table class="prices">
						<tr>
							<td>Microsoft SQL Server</td>
							<td><span class="red">590 <img src="images/ico/i-money.png" alt=""></span></td>
							<!--<td>Офисный пакет Microsoft Office</td>-->
							<!--<td><span class="red">495 <img src="images/ico/i-money.png" alt=""></span></td>-->
						</tr>
					</table>
				</div>
			</div>
		</div>
		<!-- Content #END /-->
		<!-- Footer -->
			<footer class="footer">
				<div class="wrapper">
					<div class="pull-left">
						<a href="/">
							<img src="images/logo-footer.png" alt="">
						</a>
					</div>
					<div class="pull-right">
						<p>+ 7(495) 617-61-37</p>
						<p>cloud@rit-group.ru</p>
					</div>
				</div>
			</footer>
		<!-- Footer #END /-->
		<div id="notice" class="popEvent" style="z-index:22222;margin-top:-30px;text-align:center;width:500px;margin-left:-250px;display:none;">
<div class="in">
    <div class="textMessage">{info}</div>
</div><div class="shadow">&nbsp;</div></div>
    </body>
</html>