<form class="form-horizontal" style="padding-left:25%;" method="POST">
  <div class="control-group" data-name="fio">
    <label class="control-label" for="fio">Ф.И.О.</label>
    <div class="controls">
     <input type="text" autocomplete="false" class="input-xlarge" id="fio" name="fio"  value="{fio}" placeholder="">
    </div>
</div>

  <div class="control-group">
    <label class="control-label" for="inputEmail">Логин</label>
    <div class="controls">
      <input type="text" autocomplete="false" class="input-xlarge" id="login" name="login"  value="{login}" placeholder="">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="inputpassword">Новый пароль:</label>
    <div class="controls">
      <input type="password" autocomplete="false" class="input-xlarge" id="inputpassword" name="password"  value="" placeholder="">
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="inputpassword2">Подтверждение нового пароля:</label>
    <div class="controls">
      <input type="password" autocomplete="false" class="input-xlarge" id="inputpassword2" name="password2"  value="" placeholder="">
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="inputEmail">E-mail:</label>
    <div class="controls">
      <input type="text" class="input-xlarge" id="inputEmail" name="email"  value="{email}" placeholder="">
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="inputphone">Телефон:</label>
    <div class="controls">
      <input type="text" class="input-xlarge" id="inputphone" name="phone"  value="{phone}" placeholder="">
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="mainadmin">Администратор:</label>
    <div class="controls">
      <input type="checkbox" class="input-xlarge" id="mainadmin" name="mainadmin"  value="1" {mainadmin} placeholder="">
    </div>
</div>
<div class="form-actions clear-form">
  <button type="submit" class="btn btn-primary">Сохранить</button>
  <button type="button" class="btn">Отменить</button>
</div> 
<input type="hidden" name="h" value="{hash}" />
<input type="hidden" name="subaction" value="saveinfo" />  
</form>