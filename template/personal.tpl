
<form class="form-horizontal" style="padding-left:25%;" method="POST">

    [edit_login]
    <input type="hidden" name="admin_uid" value="{admin_uid}" />
    [/edit_login]
 [edit_login]
<div class="control-group">
    <label class="control-label" for="quota">На диске:</label>
    <div class="controls">
       <input type="text" class="input-xlarge" id="quota" name="quota"  value="{quota}" placeholder="">
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="max-users">Пользователей:</label>
    <div class="controls">
       <input type="text" class="input-xlarge" id="max-users" name="max-users"  value="{max-users}" placeholder="">
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="max-users">Срок действия:</label>
    <div class="controls">
       <input type="text" class="input-xlarge" id="date" name="date" data-date-format="dd.mm.yyyy" value="{date}" placeholder="">
    </div>
</div>
[/edit_login]

<div class="control-group">
    <label class="control-label" for="inputorg">Название организации:</label>
    <div class="controls">
      <input type="text" class="input-xlarge" id="inputorg" name="org"  value='{org}' placeholder="">
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="inputinn">ИНН:</label>
    <div class="controls">
      <input type="text" class="input-xlarge" id="inputinn" name="inn"  value="{inn}" placeholder="">
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="inputkpp">КПП:</label>
    <div class="controls">
      <input type="text" class="input-xlarge" id="inputkpp" name="kpp"  value="{kpp}" placeholder="">
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="inputaddress">Юридический адрес:</label>
    <div class="controls">
      <textarea rows="3" style="width: 270px;" name="address" id="inputaddress"> {address}</textarea>
 
    </div>
</div>
<div class="form-actions clear-form">
  <button type="submit" class="btn btn-success">Сохранить</button>
  <button type="button" class="btn">Отменить</button>
</div> 
<input type="hidden" name="h" value="{hash}" />
<input type="hidden" name="sa" value="saveinfo" />  
</form>