[main]
<table class="table table-condensed table-hover">
<thead>
<tr>
<th>ID</th><th>Группа</th><th>Кол-во пользователей</th><th>Места на диске (Gb)</th><th>Группа</th>
</tr>
</thead>
{groups}
</table>
<div class="form-actions clear-form">
  <a href="?do=groups&action=editgroup&make=add&h={hash}" class="btn btn-success">Добавить</a>
</div> 
[/main]
[edit]
<form class="form-horizontal" style="padding-left:25%;" method="POST">
  <div class="control-group">
    <label class="control-label" for="name">Название группы</label>
    <div class="controls">
      <input type="text" autocomplete="false" class="input-xlarge" id="group_name" name="group_name"  value="{group_name}" placeholder="">
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="maxusers">Пользователей:</label>
    <div class="controls">
      <input type="text" autocomplete="false" class="input-xlarge" id="maxusers" name="maxusers"  value="{maxusers}" placeholder="">
    </div>
</div>
<div class="control-group input-append">
    <label class="control-label" for="disksize">Размер места на диске:</label>
    <div class="controls">
      <input type="text" autocomplete="false" class="input-large" id="disksize" name="disksize"  value="{disksize}" placeholder=""><span class="add-on">Gb</span>
    </div>
</div>
<div class="form-actions clear-form">
  <button type="submit" class="btn btn-primary">Сохранить</button>
  <button type="button" class="btn" onclick="javascript:history.back(-1)">Отменить</button>
</div> 
<input type="hidden" name="h" value="{hash}" />
<input type="hidden" name="subaction" value="{action}" />  
</form>
[/edit]