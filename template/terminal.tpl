<table class="table table-condensed table-hover">
<thead>
<tr>
<th>Логин</th><th>ФИО</th><th>Роль</th><th>Статус</th><th>Группа</th>
</tr>
</thead>
{users}
</table>

<div class="modal hide fade" style="display:none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Новый пользователь</h3>
  </div>
  <div class="modal-body">
    <form class="form-horizontal" method="POST" id="newuser-form" onsubmit="return addnewuser();">
  <div class="control-group" data-name="fio">
    <label class="control-label" for="fio">Ф.И.О.</label>
    <div class="controls">
     <input type="text" autocomplete="false" class="input-xlarge" id="fio" name="fio"  value="" placeholder="">
    </div>
</div>
  <div class="control-group" data-name="login">
    <label class="control-label" for="login">Логин</label>
    <div class="controls">
     <input type="text" autocomplete="false" title="Логин должен быть заполнен!" class="input-xlarge" id="login" name="login"  value="" placeholder="">
    </div>
</div>
<div class="control-group" data-name="pass1">
    <label class="control-label" for="password">Новый пароль:</label>
    <div class="controls">
      <input type="password" autocomplete="false" class="input-xlarge" id="password" name="password"  value="" placeholder="">
    </div>
</div>
<div class="control-group" data-name="pass2">
    <label class="control-label" for="password2">Подтверждение нового пароля:</label>
    <div class="controls">
      <input type="password" autocomplete="false" class="input-xlarge" id="password2" name="password2"  value="" placeholder="">
    </div>
    <div id="result">
    	
    </div>
</div>
<input  type="hidden" name="h" value="{hash}" />
{admin}
  </div>
  
  <div class="modal-footer">
    <a href="#" data-dismiss="modal" class="btn">Закрыть</a>
	<a href="#" class="btn btn-success" onclick="javascript:addnewuser();">Добавить пользователя</a>
    
  </div>
  </form>
</div>