<?php
if( ! defined( 'kabinet' ) ) {
	die( "Hacking attempt!" );
}
////////////
// Локализация транслита
$langtranslit = array(
'а' => 'a', 'б' => 'b', 'в' => 'v',
'г' => 'g', 'д' => 'd', 'е' => 'e',
'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
'и' => 'i', 'й' => 'y', 'к' => 'k',
'л' => 'l', 'м' => 'm', 'н' => 'n',
'о' => 'o', 'п' => 'p', 'р' => 'r',
'с' => 's', 'т' => 't', 'у' => 'u',
'ф' => 'f', 'х' => 'h', 'ц' => 'c',
'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
'ь' => '', 'ы' => 'y', 'ъ' => '',
'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
"ї" => "yi", "є" => "ye",

'А' => 'A', 'Б' => 'B', 'В' => 'V',
'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
'И' => 'I', 'Й' => 'Y', 'К' => 'K',
'Л' => 'L', 'М' => 'M', 'Н' => 'N',
'О' => 'O', 'П' => 'P', 'Р' => 'R',
'С' => 'S', 'Т' => 'T', 'У' => 'U',
'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
"Ї" => "yi", "Є" => "ye",
);
function genpass($len=9)
{
    $num = range(0, 9);
    $alf = range('a', 'z');
    $_alf = range('A', 'Z');
    $symbols = array_merge($num, $alf, $_alf);
    shuffle($symbols);
    $code_array = array_slice($symbols, 0, $len);
    $code = implode("", $code_array);
    $code=$code.ceil(rand(1,11));
    return $code;
}
function totranslit($var, $lower = true, $punkt = true) {
	global $langtranslit;
	
	if ( is_array($var) ) return "";

	$var = str_replace(chr(0), '', $var);

	if (!is_array ( $langtranslit ) OR !count( $langtranslit ) ) {
		$var = trim( strip_tags( $var ) );

		if ( $punkt ) $var = preg_replace( "/[^a-z0-9\_\-.]+/mi", "", $var );
		else $var = preg_replace( "/[^a-z0-9\_\-]+/mi", "", $var );

		$var = preg_replace( '#[.]+#i', '.', $var );
		$var = str_ireplace( ".php", ".ppp", $var );

		if ( $lower ) $var = strtolower( $var );

		return $var;
	}
	
	$var = trim( strip_tags( $var ) );
	$var = preg_replace( "/\s+/ms", "-", $var );
	$var = str_replace( "/", "-", $var );

	$var = strtr($var, $langtranslit);
	
	if ( $punkt ) $var = preg_replace( "/[^a-z0-9\_\-.]+/mi", "", $var );
	else $var = preg_replace( "/[^a-z0-9\_\-]+/mi", "", $var );

	$var = preg_replace( '#[\-]+#i', '-', $var );
	$var = preg_replace( '#[.]+#i', '.', $var );

	if ( $lower ) $var = strtolower( $var );

	$var = str_ireplace( ".php", "", $var );
	$var = str_ireplace( ".php", ".ppp", $var );

	if( strlen( $var ) > 200 ) {
		
		$var = substr( $var, 0, 200 );
		
		if( ($temp_max = strrpos( $var, '-' )) ) $var = substr( $var, 0, $temp_max );
	
	}
	
	return $var;
}

function topage($data)
{
    $data=strip_tags($data);
    $data=stripcslashes($data);
    $data=htmlspecialchars($data);
    return $data;
    
}
function msgbox($title, $text,$type='info') {
	global $tpl;
	if(!class_exists('dle_template'))
    {
        return;
    }
	$tpl_2 = new dle_template( );
	$tpl_2->dir = $tpl->dir;
	
	$tpl_2->load_template( 'info.tpl' );
	
	$tpl_2->set( '{text}', $text );
	$tpl_2->set( '{title}', $title );
	$tpl_2->set( '{type}', $type);
	$tpl_2->compile( 'info' );
	$tpl_2->clear();
	
	$tpl->result['info'] .= $tpl_2->result['info'];
	
}


function bukv($data){
$text=explode(" ",$data);
foreach($text as $dtext){
$data=strlen($dtext);
$rest .= substr($dtext, -$data,1);
}
return $rest;
}
function convert_unicode($t, $to = 'windows-1251')
{
$to = strtolower($to);

if ($to == 'utf-8') {

$t = preg_replace( '#%u([0-9A-F]{1,4})#ie', "decode_to_utf8(hexdec('\\1'))", utf8_encode($t) );
$t = urldecode ($t);

} else {

$t = preg_replace( '#%u([0-9A-F]{1,4})#ie', "'&#' . hexdec('\\1') . ';'", $t );
$t = urldecode ($t);
$t = @html_entity_decode($t, ENT_NOQUOTES, $to);

}

return $t;
}

function clean_url($url) {

	if( $url == '' ) return;

	$url = str_replace( "https://", "", $url );
	if( strtolower( substr( $url, 0, 4 ) ) == 'www.' ) $url = substr( $url, 4 );
	$url = explode( '/', $url );
	$url = reset( $url );
	$url = explode( ':', $url );
	$url = reset( $url );

	return $url;
}

function clean($data)
{
global $db;
$data=trim( strip_tags (stripslashes($data)));
$data=$db->safesql($data);
return $data;
}
$domain_cookie = explode (".", clean_url( $_SERVER['HTTP_HOST'] ));
$domain_cookie_count = count($domain_cookie);
$domain_allow_count = -2;

if ( $domain_cookie_count > 2 ) {

	if ( in_array($domain_cookie[$domain_cookie_count-2], array('com', 'net', 'org') )) $domain_allow_count = -3;
	if ( $domain_cookie[$domain_cookie_count-1] == 'ua' ) $domain_allow_count = -3;
	$domain_cookie = array_slice($domain_cookie, $domain_allow_count);
}
$domain_cookie = ($_SERVER['HTTP_HOST'] != 'localhost') ? implode (".", $domain_cookie) : false;
//$domain_cookie=false;
function set_cookie($name, $value, $expires) {
global $domain_cookie ;
	if( $expires ) {

		$expires = time() + ($expires * 86400);

	} else {

		$expires = FALSE;

	}

	if( PHP_VERSION < 5.2 ) {

		setcookie( $name, $value, $expires, "/", $domain_cookie . "; HttpOnly" );
	//	setcookie( $name, $value, time() + ($expires * 86400), "/");

	} else {

		setcookie( $name, $value, $expires, "/", $domain_cookie, NULL, TRUE );
	//setcookie( $name, $value, $expires, "/");

	}


}

//VARS
function set_vars($file, $data) {

	$fp = fopen( MOD_DIR . '/cache/' . $file . '.php', 'wb+' );
	fwrite( $fp, serialize( $data ) );
	fclose( $fp );

	@chmod( MOD_DIR . '/cache/' . $file . '.php', 0666 );
}

function get_vars($file) {

	return unserialize( @file_get_contents( MOD_DIR . '/cache/' . $file . '.php' ) );
}
function build_navigation( $url, $count, $max_com, $page='' ) {
		global $tpl, $config, $lang; 
        $url=$PHP_SELF.$url;
		if( $count < $max_com ) return;

		if( !$page OR $page < 1 ) $page = 1;

		//----------------------------------
		// Предыдущая страница
		//----------------------------------
		if( $page > 1 ) {
			$prev = $page - 1;

			$link = str_replace('{page}', $prev, $url);
	
			$result_tpl .= "<li><a href=\"" . $link . "\"> << </a></li>";
		
		} else {
			$no_prev = TRUE;
		}

		//----------------------------------
		// страницы
		//----------------------------------
		if( $max_com ) {
			
			$enpages_count = @ceil( $count / $max_com );
			$pages = "";
			
			if( $enpages_count <= 10 ) {
				
				for($j = 1; $j <= $enpages_count; $j ++) {
					
					if( $j != $page  ) {
						
						$link = str_replace('{page}', $j, $url);
						$pages .= "<li><a href=\"" . $link . "\">$j</a></li> ";
					
					} else {
						
						$pages .= "<li class=\"disabled\"><span>$j</span></li>";
					}
				
				}
			
			} else {
				
				$start = 1;
				$end = 10;
				$nav_prefix = "<li class=\"disabled\"><span class=\"\">...</span></li>";
				
				if( $page  > 0 ) {
					
					if( $page  > 6 ) {
						
						$start = $page  - 4;
						$end = $start + 8;
						
						if( $end >= $enpages_count ) {
							$start = $enpages_count - 9;
							$end = $enpages_count - 1;
							$nav_prefix = "";
						} else
							$nav_prefix = "<li class=\"disabled\"><span class=\"nav_ext\">...</span></li>";
					
					}
				
				}
				
				if( $start >= 2 ) {

						$link = str_replace('{page}', '1', $url);
					
						$pages .= "<li><a href=\"" . $link . "\">1</a> <span class=\"nav_ext\">...</span></li> ";
				
				}
				
				for($j = $start; $j <= $end; $j ++) {
					
					if( $j != $page ) {
						
							$link = str_replace('{page}', $j, $url);
							$pages .= "<li><a href=\"" . $link . "\">$j</a></li> ";
					
					} else {
						
						$pages .= "<li class=\"disabled\"><span>$j</span></li>";
					}
				
				}
				
				if( $page != $enpages_count ) {
					
					$link = str_replace('{page}', $enpages_count, $url);
					$pages .= $nav_prefix . "<li><a href=\"" . $link . "\">{$enpages_count}</a></li>";
				
				} else
					$pages .= "<li class=\"disabled\"><span>{$enpages_count}</span></li>";
			
			}
			
			$result_tpl .= $pages;
		
		}

		//----------------------------------
		// следующая страница
		//----------------------------------
		if( $page < $enpages_count ) {


			$next_page = $page + 1;

				$link = str_replace('{page}', $next_page, $url);
				$result_tpl .= "<li><a href=\"" . $link . "\"> >> </a></li>";

		} else {
			$no_next = TRUE;
		}

	return ("<div class=\"pagination pagination-centered\"><ul>".$result_tpl."</ul></div>");
}
