<?php
if (!defined('kabinet')) {
    die("Hacking attempt!");
}
if ($member_id['group_id'] != 1) {
    die("Error. No members");
}
$tpl->load_template("groups.tpl");

if ($_REQUEST['action'] == "editgroup") {
    if ($_REQUEST['h'] == "" || $_REQUEST['h'] !== $login_hash) {
        die('some error');
    }
    if ($_REQUEST['make'] == "add") {
        if ($_POST['subaction'] == "newsave") {
            $name = $db->safesql($_POST['group_name']);
            $max_users = intval($_POST['maxusers']);
            $disk_size = intval($_POST['disksize']);
            $db->query("INSERT into " . PREFIX . "_group (`name`,`max_users`,`disk_size`)VALUES('$name','$max_users','$disk_size')");
            msgbox("Добавлено!", "Группа была успешно добавлена!");
        }

        $tpl->set("{group_name}", "");
        $tpl->set("{maxusers}", "");
        $tpl->set("{disksize}", "");
        $tpl->set("{action}", "newsave");
        $tpl->set_block("'\\[edit\\](.*?)\\[/edit\\]'si", "\\1");
        $tpl->set_block("'\\[main\\](.*?)\\[/main\\]'si", "");

    } else {
        $uid = intval($_REQUEST['uid']);
        $row = $db->super_query("SELECT * FROM " . PREFIX . "_group where id='{$uid}'");
        if ($row['id']) {
            if ($_REQUEST['subaction'] == "editsave") {
                $row['name'] = $db->safesql($_POST['group_name']);
                $row['max_users'] = intval($_POST['maxusers']);
                $row['disk_size'] = intval($_POST['disksize']);
                $db->query("UPDATE " . PREFIX . "_group set name='{$row['name']}',max_users='{$row['max_users']}',disk_size='{$row['disk_size']}' where id='{$row['id']}'");
                msgbox("”спешно!", "?руппа обновлена!", "success");
            }
            $tpl->set("{group_name}", stripslashes($row['name']));
            $tpl->set("{maxusers}", intval($row['max_users']));
            $tpl->set("{disksize}", $row['disk_size']);
            $tpl->set("{action}", "editsave");

            $tpl->set_block("'\\[edit\\](.*?)\\[/edit\\]'si", "\\1");
            $tpl->set_block("'\\[main\\](.*?)\\[/main\\]'si", "");
        }
    }
} else {


    $db->query("SELECT * FROM " . PREFIX . "_group order by id asc");

    while ($row = $db->get_row()) {
        $act = "<li><a href=\"?do=groups&action=editgroup&uid={$row['id']}&h=$login_hash\">Редактировать</a></li>";
        $act .= "<li><a href=\"?do=groups&action=delete&uid={$row['id']}&h=$login_hash\">Удалить</a></li>";
        $groups .= <<<HTML
	<tr>
<td>{$row['id']}</td><td>{$row['name']}</td><td>{$row['max_users']}</td><td>{$row['disk_size']}</td><td><div class="btn-group"><a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Действие
    <span class="caret"></span>
  </a>
  <ul class="dropdown-menu">
$act
  </ul>
</div></td>
</tr>
HTML;
    }

    $tpl->set_block("'\\[edit\\](.*?)\\[/edit\\]'si", "");
    $tpl->set_block("'\\[main\\](.*?)\\[/main\\]'si", "\\1");
    $tpl->set("{groups}", $groups);
}
$tpl->set("{hash}", $login_hash);
$tpl->compile("content");