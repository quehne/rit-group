<?php
if (!defined('kabinet')) {
    die("Hacking attempt!");
}
if ($member_id['group_id'] != 1 and $member_id['group_id'] != 2) {
    die("Error. No members");
}
if ($_GET['action'] == "delete" and $member_id['group_id'] == 1 and $_REQUEST['h'] != "" and $_REQUEST['h'] == $login_hash) {
    require_once(MOD_DIR . "/class/connect.class.php");

    $uid = intval($_GET['company']);
    $row = $db->super_query("SELECT id FROM " . PREFIX . "_company where id='{$uid}'");
    if ($row['id']) {
        $db->query("DELETE FROM " . PREFIX . "_users where company='{$row['id']}'");
        $db->query("DELETE FROM " . PREFIX . "_company where id='{$row['id']}'");
        $del_comp = new WinConnect();
        $del_comp->build(array("action" => "deletecomp", "group" => "group" . $row['id']));
        msgbox("Удалено!", "Компания была удалена!", "success");
    } else msgbox("Ошибка!", "Компания не найдена удалена!", "error");
}
if ($member_id['group_id'] == 2 and $member_id['manager_code'] == "") {
    msgbox("Ошибка!", "У Вас нет персонального кода !", "error");
} else {
    if ($member_id['group_id'] == 2 and $member_id['manager_code'] != "") {
        $where_sql = " AND promo='{$member_id['manager_code']}'";
        $tpl->load_template("users.manager.tpl");

    } else {
        $tpl->load_template("users.tpl");

    }
    $query = $db->query("SELECT u.*,g.name as group_name,c.org as company_name,c.regdate,c.id as comp_id,c.max_users,c.end_date,c.quota FROM " . PREFIX . "_users u LEFT JOIN " . PREFIX . "_company c on c.id=u.company LEFT JOIN " . PREFIX . "_group g on g.id=c.group_id where u.main_admin=1 $where_sql group by company order by u.id asc");
    while ($row = $db->get_row($query)) {
        $row['name'] = stripslashes(htmlspecialchars($row['name'], NULL, "utf-8"));
        $row['org'] = stripslashes(htmlspecialchars($row['org'], NULL, "utf-8"));
        $act = "<li><a href=\"?do=terminal&m=admin&company={$row['comp_id']}\">Пользователи</a></li>";
        $act .= "<li><a href=\"?do=info&m=admin&company={$row['comp_id']}\">Параметры</a></li> <li class=\"divider\"></li>
";
        $days_left = $row['end_date'] - time();
        $days_left = ceil($days_left / 86400);
        if ($days_left < 14) $bg = "class=\"warning\""; else $bg = "";
        $act .= "<li><a href=\"?do=users&m=admin&company={$row['comp_id']}&action=delete&h={$login_hash}\">Удалить группу</a></li>";
        $users_num = $db->super_query("SELECT count(id) as count FROM " . PREFIX . "_users where active='1' AND company='{$row['comp_id']}'");
        $user_ac = $users_num['count'];
        $user_total = $row['max_users'];
        $date_end = date("d.m.y", $row['end_date']);
        if ($member_id['group_id'] == 2) {
            $act = "";
            $users .= <<<HTML
    <tr $bg><td>{$row['company_name']}</td><td>{$row['regdate']}</td><td>{$date_end} ($days_left)</td><td>{$row['name']}</td><td>{$row['email']}</td><td>{$row['phone']}</td><td>{$user_ac}/{$user_total}</td><td>{$row['quota']}Gb</td></tr>
HTML;

        } else {
            $act = <<<HTML
    <div class="btn-group"><a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Действие<span class="caret"></span></a>
    <ul class="dropdown-menu">$act</ul></div>    
HTML;

            /* ///огранизации, Дата регистрации, Дата окончания действия услуги, ФИО контактного лица, Телефон конт. лица */
            $users .= <<<HTML
    <tr $bg><td>{$row['comp_id']}</td><td>{$row['company_name']}</td><td>{$row['regdate']}</td><td>{$row['name']}</td><td>{$row['email']}</td><td>{$row['phone']}</td><td>{$date_end} ($days_left)</td><td>{$user_ac}/{$user_total}</td><td>{$row['quota']}Gb</td><td>$act</td></tr>
HTML;
        }
    }
}
$title = "Пользователи";
$tpl->set("{users}", $users);
$tpl->compile("content");