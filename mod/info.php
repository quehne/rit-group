<?php
if( ! defined( 'kabinet' ) ) {
	die( "Hacking attempt!" );
}

if(!USER_LOGGED)
{
    @header("Location: ".$config['http_home_url']);
}


function cls($data)
{
    #return $data;
    return htmlspecialchars(stripcslashes($data),ENT_QUOTES,"utf-8");
}
function toDB($data)
{
    global $db;
    $data=strip_tags(trim($data));
    $data=$db->safesql($data);
    return $data;
}
 	require_once(MOD_DIR."/class/connect.class.php");

if($_POST['sa']=="saveinfo" AND isset($_POST['h'])AND $_POST['h']==$login_hash)
{
 $name=toDB($_POST['fio']);
 $email=toDB($_POST['email']);
 $phone=toDB($_POST['phone']);
 $inn=toDB($_POST['inn']);
 $kpp=toDB($_POST['kpp']);
 $address=toDB($_POST['address']);
 $org=toDB($_POST['org']);

 if($_GET['m']=="admin" && $member_id['group_id']==1)
 {
		$member_id['company']=intval($_GET['company']);
		$login=toDB($_POST['login']);
		$member_id['id']=intval($_POST['admin_uid']);
		$mem=$db->super_query("SELEct id,end_date,quota,path from ".PREFIX."_company where id='{$member_id['company']}'");
		#$group="group_id='".intval($_POST['group'])."',";
		$max_users=intval($_POST['max-users']);
		$quota=intval($_POST['quota']);
		$login="username='{$login}',";
		$end_date=strtotime($_POST['date']);
		if($end_date<time())$end_date=time();
		$prams_acc="quota='$quota',max_users='$max_users',end_date='$end_date',";
		if($mem['end_date']!=$end_date)
		{
			$params=array("editexp"=>true,"date"=>date("d.m.Y",$end_date),"group"=>"group".$member_id['company']);

		}
		if($mem['quota']!=$quota)
		{
			$params=array("quota"=>$quota,"newquota"=>true,"path"=>"{$mem['path']}");
			
		}
		if($params){
		
			$newdate=new WinConnect();
			$params['action']="edituser";
			$newdate->build($params);
		}		
		
 }
 if(trim($_POST['password'])!="" AND $_POST['password']==$_POST['password2'])
 {
	 $password="password='".md5(md5($_POST['password']))."',";
 }elseif(trim($_POST['password'])!=""){
	 $stop.="<li>Пароли не совпадают!</li>";
 }
 if(!$stop){
 #$db->query("UPDATE ".PREFIX."_users set $login name='{$name}',$password email='{$email}' WHERE id='{$member_id['id']}'");
 $db->query("UPDATE ".PREFIX."_company set $prams_acc org='$org',inn='$inn',kpp='$kpp',phone='$phone',address='$address' where id='{$member_id['company']}'");
 if($password)
 {
 	$connect=new WinConnect();
	$params=array('action'=>"edituser","login"=>"{$member_id['username']}");
	$params["password"]=$_POST['password'];
	$params["changepass"]=true;
	$connect->build($params);
 }
 msgbox("Успешно!","Параметры успешно сохранены!","success");
 }else{
msgbox("Ошибка!","<ul>$stop</ul>","error");	 
 }
}
$tpl->load_template("personal.tpl");

if($member_id['group_id']==1 AND $_GET['m']=="admin")
{
	$member_id['company']=intval($_GET['company']);
	$row=$db->super_query("SELECT u.*,u.id as uid,c.group_id as gid, c.* FROM ".PREFIX."_users u left join ".PREFIX."_company c on c.id=u.company where u.main_admin=1 and c.id='{$member_id['company']}'");
	$db->query("SELECT * FROM ".PREFIX."_group order by id asc");
		
		while($group=$db->get_row())
		{
			if($group['id']==$row['gid'])$sel="selected";else $sel="";
			$option.="<option value=\"{$group['id']}\" $sel>{$group['name']}</option>";
		}
$tpl->set("{groups}",$option);
$tpl->set_block ( "'\\[edit_login\\](.*?)\\[/edit_login\\]'si", "\\1" );
$tpl->set_block ( "'\\[login\\](.*?)\\[/login\\]'si", "" );


}else
{

	$row=$db->super_query("SELECT u.*,c.* FROM ".PREFIX."_users u left join ".PREFIX."_company c on c.id=u.company where u.id='{$member_id['id']}'");
	$tpl->set_block ( "'\\[edit_login\\](.*?)\\[/edit_login\\]'si", "" );
	$tpl->set_block ( "'\\[login\\](.*?)\\[/login\\]'si", "\\1" );

}
$tpl->set("{date}",date("d.m.Y",$row['end_date']));
$tpl->set("{quota}",$row['quota']);
$tpl->set("{max-users}",$row['max_users']);
$tpl->set("{login}",cls($row['username']));
$tpl->set("{name}",cls($row['name']));
$tpl->set("{email}",cls($row['email']));
$tpl->set("{phone}",cls($row['phone']));
$tpl->set("{org}",cls($row['org']));
$tpl->set("{hash}",$login_hash);
$tpl->set("{inn}",cls($row['inn']));
$tpl->set("{kpp}",cls($row['kpp']));
$tpl->set("{address}",cls($row['address']));
$tpl->set("{admin_uid}",$row['uid']);
//$tpl->set("{name}",stripcslashes($row['name']));
$tpl->compile("content");
