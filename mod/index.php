<?php
@session_start ();
@ob_start ();
@ob_implicit_flush ( 0 );
@error_reporting ( E_ALL ^ E_NOTICE );
@ini_set ( 'display_errors', true );
@ini_set ( 'html_errors', false );
@ini_set ( 'error_reporting', E_ALL ^ E_NOTICE );
define ( 'kabinet', true );

define ( 'ROOT_DIR', dirname ( __FILE__ ) );
define ( 'MOD_DIR', ROOT_DIR . '/mod' );
$config['http_home_url']="https://cloud.rit-group.ru/";
require_once MOD_DIR ."/class/mysql.php";
require_once MOD_DIR ."/class/db.conf.php";
require_once ROOT_DIR . '/mod/function.php';

require_once MOD_DIR . '/class/templates.class.php';
$tpl = new dle_template();
$tpl->dir=ROOT_DIR."/template/";
$member_id['group']=5;
require_once ROOT_DIR . '/mod/auth.php';
$action=(trim($_REQUEST['do'])!="")?trim($_REQUEST['do']):"";
if(!USER_LOGGED)
{
if($action=="connect")
   if($action=='connect')
   require_once MOD_DIR."/connect.php";
     
    if($action=="register")
    $tpl->load_template("register.tpl");
    else
    $tpl->load_template("login.tpl");
    $tpl->compile('content');
    $title="Авторизация";
}elseif(USER_LOGGED)
{
    
    switch($action)
    { 
        case "users":
        require_once MOD_DIR."/users.php";
        break;
        case "index":
        require_once MOD_DIR."/index.php";
        break;
        case "info":
        require_once MOD_DIR."/info.php";
        break;
        case "terminal":
        require_once MOD_DIR."/terminal.php";
        break;
        case "groups":
        require_once MOD_DIR."/groups.php";
        break;
		case "manager":
        require_once MOD_DIR."/manager.php";
        break;
        case "connect":
        require_once MOD_DIR."/connect.php";
        die();
        break;
       // case "main":
        //require_once MOD_DIR."/main.php";
        //break;
        //default:
        //require_once MOD_DIR."/main.php";
    }
    require_once MOD_DIR."/main.php";

    
}
if($action=="")
{
$tpl->load_template('_index.tpl');
$tpl->result['info']=preg_replace("/\[nomain\](.*)?\[\/nomain\]/si","",$tpl->result['info']);
$tpl->result['info']=preg_replace("/\[main\](.*)?\[\/main\]/si","\\1",$tpl->result['info']);
}else
{
    $tpl->result['info']=preg_replace("/\[nomain\](.*)?\[\/nomain\]/si","\\1",$tpl->result['info']);
    $tpl->result['info']=preg_replace("/\[main\](.*)?\[\/main\]/si","",$tpl->result['info']);
    $tpl->load_template("main.tpl");
}

if(!USER_LOGGED)
{
    $tpl->set("[not-logged]","");
    $tpl->set("[/not-logged]","");
    $tpl->set_block ( "'\\[logged\\](.*?)\\[/logged\\]'si", "" );
}else{
	$tpl->set("{login}",$member_id['name']);
    $tpl->set("[logged]","");
    $tpl->set("[/logged]","");
    $tpl->set_block ( "'\\[not-logged\\](.*?)\\[/not-logged\\]'si", "" );
    
}

$tpl->set("{content}",$tpl->result['content']);
$tpl->set("{title}",$title);
$tpl->set("{menu}",$menu);
$tpl->set("{info}",$tpl->result['info']);
$tpl->compile('main');
$tpl->result['main']=str_replace("{THEME}",$config['http_home_url']."template",$tpl->result['main']);
echo $nav.$tpl->result['main'].$nav;