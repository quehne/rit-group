<?php
if( ! defined( 'kabinet' ) ) {
	die( "Hacking attempt!" );
}
if($member_id['group_id']!=2)$m['main'][]="Главная";
if($member_id['group_id']!=1 and $member_id['main_admin']==1)
{

$m['terminal'][]="Управление пользователями";
$m['info'][]="Параметры";

}
if($member_id['group_id']==1)
{
    $m['users'][]="Администрирование";
    $m['manager'][]="Менеджеры";
    $m["terminal&action=edituser&uid=1&h={$login_hash}"][]="Личные настройки";
    if(($action=="terminal"||$action=="info") && $_GET['uid']!=1)
    {
	    $cid=intval($_REQUEST['company']);
   	    $m["terminal&m=admin&company={$cid}"][]="Пользователи компании";
	    $m["info&m=admin&company={$cid}"][]="Параметры";
    }
    #$m['groups'][]="Группы";
    $m['servers'][]="Серверы";
}
if($member_id['group_id']==2)
{
    $m['users'][]="Мои клиенты";
}
foreach ($m as $href=>$item)
{
    
   if($href===$action)$active="active";else $active="";
   
   
    if($href)
    {
        $menu.="<li class=\"$active\"><a class=\"auth\"  href='{$config['http_home_url']}?do={$href}'><span>$item[0]</span></a></li>";    
    }else{
        $menu.="<li class=\"$active\"><a class=\"auth\" href='{$config['http_home_url']}'><span>$item[0]</span></a></li>";
    }
    
}
$menu.='<li style="float:right;"><a class="auth" href="/?action=logout"><span>Выход</span></a></li>';

if(USER_LOGGED && $action=="main"){
	$tpl->load_template('start.tpl');
	if($member_id['group_id']==1)
	{
		$total_groups=$db->super_query("SELECT count(id) as count,sum(quota) as quota,sum(max_users)as max_users FROM ".PREFIX."_company");
		$tpl->set_block ( "'\\[admin\\](.*?)\\[/admin\\]'si", "\\1" );
		$tpl->set("{groups}",$total_groups['count']);
		$tpl->set("{size}",$total_groups['quota']);
		$users=$db->super_query("SELECT count(id)as count FROM ".PREFIX."_users where active='1' and group_id!=1");
		$users_all=$db->super_query("SELECT count(id)as count FROM ".PREFIX."_users where group_id!=1");
		$tpl->set("{users_active}",$users['count']);
		$tpl->set("{users}",$total_groups['max_users']);
		$tpl->set_block ( "'\\[user\\](.*?)\\[/user\\]'si", "" );
		
		//echo ("SELECT count(id) as count,sum(quota) as quota FROM ".PREFIX."_company where id!=1");
	}else
	{
		$tpl->set_block ( "'\\[admin\\](.*?)\\[/admin\\]'si", "" );
		$tpl->set_block ( "'\\[user\\](.*?)\\[/user\\]'si", "\\1" );
		
	$days_left=$company['end_date']-time();
	$days_left=ceil($days_left/86400);
	$tpl->set("{days_left}",$days_left);
	$tpl->set("{max_users}",$company['max_users']);
	$curr_users=$db->super_query("SELECT count(*) as count from ".PREFIX."_users where company='{$member_id['company']}'");
	$curr_users=$curr_users['count'];
	$tpl->set("{curr_users}",$curr_users);
	$tpl->set("{quota}",$company['quota']);
	$tpl->set("{download}","?do=connect&h={$login_hash}");
	$tpl->set("{max_users}",$company['max_users']);
	}
	$tpl->compile('content');	
}

