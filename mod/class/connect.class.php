<?php

class WinConnect
{
    private $api = "http://195.211.102.159/cabinet/index.php";
    private $json = false;

    public function __construct()
    {
        if (defined('WIN_API_URI'))
            $this->api  = WIN_API_URI;
    }

    private function request()
    {
        $this->query = http_build_query($this->param);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->api . "?" . $this->query);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        if ($this->proxy != "") curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        if ($this->json)
            return json_decode($result, true);
        else
            return $result;

    }

    public function build($params = array())
    {
        if (is_array($params)) {
            foreach ($params as $id => $value) {
                $this->param[$id] = $value;
            }
        }
        return $this->request();

    }
}
