<?php
if (!defined('kabinet')) {
    die("Hacking attempt!");
}

if (!USER_LOGGED) {
    @header("Location: " . $config['http_home_url']);
}


if ($_GET['action'] == "delete") {
    if ($_REQUEST['h'] == "" || $_REQUEST['h'] != $login_hash) {
        msgbox("Ошибка", "Возникла ошибка!", "error");

    } else {
        if ($member_id['group_id'] == 1 && $_GET['m'] == "admin") {
            $member_id['company'] = intval($_GET['company']);
        }

        $uid = intval($_GET['uid']);
        $row = $db->super_query("SELECT * FROM " . PREFIX . "_users where id='{$uid}' AND company='{$member_id['company']}'");
        if (!$row['id']) {
            msgbox("Ошибка", "Пользователь не найден!", 'error');
        } elseif ($row['main_admin'] and $member_id['group_id'] != 1) {
            msgbox("Ошибка", "Данный пользователь имеет права администратора!<br />Для удаление обратитесь в службу поддрежки!", 'error');
        } else {

            $db->query("DELETE FROM " . PREFIX . "_users where id='{$row['id']}' AND company='{$row['company']}'");
            msgbox("Инфо", "Пользователь удален!!", 'success');

        }

    }
} elseif ($_GET['action'] == "edituser" and intval($_GET['uid']) != 0) {
    if ($_REQUEST['h'] == "" || $_REQUEST['h'] != $login_hash) {
        msgbox("Ошибка", "Возникла ошибка!", "error");

    } else {


        $uid = intval($_GET['uid']);
        if ($_GET['m'] == "admin" && $member_id['group_id'] == 1) {
            $member_id['company'] = intval($_GET['company']);
            $admin_location = "&m=admin&company={$member_id['company']}";
            //$row=$db->super_query("SELECT * FROM ".PREFIX."_users where id='{$uid}' AND company='{$comp}'");
        }
        $row = $db->super_query("SELECT * FROM " . PREFIX . "_users where id='{$uid}' AND company='{$member_id['company']}'");
        $curr_Log = $row['username'];

        if (!$row['id']) {
            msgbox("Ошибка", "Пользователь не найден!", 'error');
        } else {
            if ($_POST['subaction'] == "saveinfo") {
                $fio = $db->safesql(trim(strip_tags($_POST['fio'])));
                $login = $db->safesql(trim(strip_tags($_POST['login'])));
                $email = $db->safesql(trim(strip_tags($_POST['email'])));
                $phone = $db->safesql(trim(strip_tags($_POST['phone'])));
                if ($_POST['password'] != "" and $_POST['password'] != $_POST['password2']) {
                    $stop .= "<li>Пароли не совпадают!</li>";
                } elseif ($_POST['password'] != "" and !preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#", $_POST['password'])) {
                    $stop .= "<li>Пароль не отвечает требование безопастности! <br /><strong>Пароль должен содержать минимум</strong>:
<ul><li>1 Заглавную Латинскую Букву</li>
<li>1 Строчную Латинскую Букву</li>
<li>1 Цифру!</li>
<li>Минимальная длина пароля 8 символов! </li></ul></li>";
                } elseif ($_POST['password'] == $_POST['password2'] and $_POST['password'] != "") {
                    $pass = md5(md5($_POST['password']));
                    $pas = ",password='$pass'";
                }
                if ($login == "") {
                    $stop .= "<li>Логин не может быть пустым!</li>";
                } elseif ($login != $row['username']) {
                    $check_login = $db->super_query("SELECT id FROM " . PREFIX . "_users where username='{$login}'");
                    if ($check_login['id']) {
                        $stop .= "<li>Пользователь $login существует!</li>";
                    }
                }
                if (!$stop) {
                    $main_admin = intval($_POST['mainadmin']);

                    $db->query("UPDATE " . PREFIX . "_users set username='{$login}'$pas,phone='{$phone}',main_admin='{$main_admin}',name='{$fio}',email='{$email}' where id='{$row['id']}' AND company='{$row['company']}'");
                    //////////UPDATE TERMINAL
                    require_once(ROOT_DIR . "/mod/class/connect.class.php");
                    $connect = new WinConnect();

                    $params = array('action' => "edituser", "login" => "$login");
                    if ($curr_Log != $login) {
                        $params["oldlogin"] = $curr_Log;

                    }
                    if ($pas) {
                        $params["password"] = $_POST['password'];
                        $params["changepass"] = true;
                    }
                    //var_dump($params);
                    if ($uid != 1) $connect->build($params);
                    //////////UPDATE TERMINAL
                    @header("LOCATION: /?do=terminal{$admin_location}");
                    msgbox("Успешно!", "Инормация о пользователе обновлена!", "success");
                    $row = $db->super_query("SELECT * FROM " . PREFIX . "_users where id='{$uid}' AND company='{$member_id['company']}'");

                } else {

                    msgbox("Во время сохранения возникли следующие ошибки!", "<ul>$stop</ul>", "error");
                }

            }

            $tpl->load_template("edituser.tpl");
            $tpl->set("{login}", stripslashes($row['username']));
            $tpl->set("{fio}", stripslashes($row['name']));
            $tpl->set("{email}", stripslashes($row['email']));
            $tpl->set("{phone}", stripslashes($row['phone']));
            $tpl->set("{hash}", $login_hash);
            if ($row['main_admin']) {
                $main_admin = "checked";
            }
            $tpl->set("{mainadmin}", $main_admin);

        }
    }
} else {
    $tpl->load_template("terminal.tpl");
    if ($_GET['status']) {
        $uid = intval($_GET['uid']);
        if ($_GET['m'] == "admin" && $member_id['group_id'] == 1) {
            $member_id['company'] = intval($_GET['company']);
            //$row=$db->super_query("SELECT * FROM ".PREFIX."_users where id='{$uid}' AND company='{$comp}'");
        }
        $row = $db->super_query("SELECT * FROM " . PREFIX . "_users where id='{$uid}' AND company='{$member_id['company']}'");

        switch ($_GET['status']) {
            case "on":
                $status = 1;
                $checknum = $db->super_query("SELECT count(*) from " . PREFIX . "_users where company='{$member_id['company']}'");
                if ($checknum['count'] > $company['max_users']) {
                    $ok = false;
                    msgbox("Ошибка!", "Превышено количество активных пользователей!");
                } else    $ok = true;
                break;
            case "off":
                $status = 0;
                $ok = true;
                break;
        }
        if ($ok and $row['id']) {

            $db->query("UPDATE " . PREFIX . "_users set active='{$status}' where id='{$row['id']}' AND company='{$row['company']}'");
            msgbox("Success", "", "success");
            require_once(ROOT_DIR . "/mod/class/connect.class.php");
            $connect = new WinConnect();
            $params = array('action' => "enable", "login" => "{$row['username']}", "status" => $status);
            $connect->build($params);
        }
    }
    if ($_GET['m'] == "admin" and $member_id['group_id'] == 1) {

        $comp = intval($_GET['company']);
        $tpl->set("{admin}", "<input type='hidden' name='company' value='{$comp}' />");
        $db->query("SELECT * FROM " . PREFIX . "_users where company='$comp' ");
        $admin = "&m=admin&company=$comp";
    } else {
        $db->query("SELECT * FROM " . PREFIX . "_users where company='{$member_id['company']}'");
        $tpl->set("{admin}", "");
    }
    $info = <<<HTML
    <tr><td colspan="4"></td><td><a href="#" onclick="javascript:$('.modal').modal()"  class="btn">Добавить</a></td></tr>
HTML;

    while ($row = $db->get_row()) {
        if ($row['main_admin']) {
            $role = "Администратор";
        } else $role = "Пользователь";
        $row['name'] = stripslashes($row['name']);
        $row['group_name'] = stripslashes($row['group_name']);
        $act = "<li><a href=\"/?do=terminal&action=edituser$admin&uid={$row['id']}&h=$login_hash\">Редактировать</a></li>";
        $act .= "<li><a href=\"javascript:void(0);\" onclick=\"javascript:terminal('{$row['id']}','logout');\">Завершить сеанс</a></li>";

        if ($row['active']) {
            $stat = "Активный";
            $act .= "<li><a class='error' href=\"/?do=terminal$admin&status=off&uid={$row['id']}&h=$login_hash\">Отключить</a></li>";
        } else {
            $act .= "<li><a class='error' href=\"/?do=terminal$admin&status=on&uid={$row['id']}&h=$login_hash\">Включить</a></li>";
            $stat = "Отключен";
        }
        if (!$row['main_admin']) $act .= "<li><a class='error' href=\"/?do=terminal$admin&action=delete&uid={$row['id']}&h=$login_hash;\" onclick=\"javascript:confirmdel('{$row['id']}');return false;\">Удалить</a></li>";
        $info .= <<<HTML
    <tr id="{$row['id']}"><td>{$row['username']}</td><td>{$row['name']}</td><td>{$role}</td><td>{$stat}</td><td><div class="btn-group"><a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
    Действие
    <span class="caret"></span>
  </a>
  <ul class="dropdown-menu">
$act
  </ul>
</div></td></tr>
HTML;

    }
    $tpl->set("{hash}", $login_hash);
    $tpl->set("{users}", $info);
}
$tpl->compile('content');
