$(function () {	
	var $originalValue = '';
 
	 swaps = [];
    $("input").each(function(i) {
        swaps[i] = $(this).val();
       
        $(this).focusin(function() {
            if ($(this).val() == swaps[i]) {
                $(this).val("");
            }
        }).focusout(function() {
            if ($.trim($(this).val()) == "") {
                $(this).val(swaps[i]);
            }
        });
    });
	$(".arrow-button").click(function () {
		$(".navigation ul ul").toggle();
	});

});