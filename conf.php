<?php

define('RIT_API_URI', 'http://195.211.102.159/cabinet/index.php');
define('MD_API_URI', 'http://10.2.4.55/cabinet/index.php');

define('MD_LOCAL', (in_array($_SERVER['REMOTE_ADDR'], ['::1'])));
define('WIN_API_URI', MD_LOCAL ? MD_API_URI : RIT_API_URI);

define('ROOT_DIR', dirname(__FILE__));
define('MOD_DIR', ROOT_DIR . '/mod');

$config['http_home_url'] = (MD_LOCAL ? "http://cloud.rit:8888/" : "https://dev.cloud.rit-group.ru/");